package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="users")
public class User {
    // User model properties:
    // Id - Long/Primary key/Auto-increments
    @Id
    @GeneratedValue
    private Long id;
    // username - String
    @Column
    private String username;
    // password - String
    @Column
    private String password;

    @OneToMany(mappedBy = "user")
    @JsonIgnore // @JsonIgnore is added to avoid infinite nesting when retrieving post content
    private Set<Post> posts;

    public Set<Post> getPosts(){
        return posts;
    }

    // constructors
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }
    // Getters and setters for username and password
    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }

    public void setUsername(String username){
        this.username = username;
    }
    public void setPassword(String password){
        this.password = password;
    }
    // Getter for Id
    public Long getId(){
        return id;
    }
}
